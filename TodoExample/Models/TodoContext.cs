using Microsoft.EntityFrameworkCore;

namespace TodoExample.Models
{
    public class TodoContext : DbContext
    {
        public DbSet<TodoItem> Todos { get; set; }
        public DbSet<Category> Categories { get; set; }

        public TodoContext(DbContextOptions<TodoContext> dbContextOptions) : 
            base(dbContextOptions)
        {
        }
    }
}