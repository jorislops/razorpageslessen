using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TodoExample.Models
{
    public class TodoItem
    {
        //[Key]
        public int TodoItemId { get; set; }
        [Required(ErrorMessage = "Beschrijving mag niet leeg zijn"), 
         MinLength(2, ErrorMessage = "Lengte moet minimaal twee zin"),
         Display(Name = "Beschrijving")]
        public string Description { get; set; }

        [Required, DefaultValue(false), Display(Name = "Gedaan?")]
        public bool Done { get; set; }
        
        [Required, 
         Display(Name = "Voldaan voor"),
         DataType(DataType.Date)
        ]
        public DateTime DueDate { get; set; }


        [Display(Name = "Categorie")]
        public int CategoryId { get; set; }
        
        public Category Category { get; set; }
    }

    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}