using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoExample.Models;

namespace TodoExample.ViewComponents
{
    public class CategoryMenuComponent : ViewComponent
    {
        private readonly TodoContext _db;

        public CategoryMenuComponent(TodoContext db)
        {
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(_db.Categories);
        }
    }
}