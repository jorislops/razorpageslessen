using System;
using TodoExample.Models;

static internal class DbSeed
{
    public static void Seed(TodoContext db)
    {
        var category1 = new Category() { Name = "Category 1"};
        var category2 = new Category() { Name = "Category 2"};
        
        db.Todos.Add(new TodoItem()
        {
            Description = "Item 1", Done = false, DueDate = DateTime.Now,
            Category = category1
        });
        db.Todos.Add(new TodoItem()
        {
            Description = "Item 2", Done = true, DueDate = DateTime.Now.AddDays(2),
            Category = category2
        });
        db.SaveChanges();
    }
}