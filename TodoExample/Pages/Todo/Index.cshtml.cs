using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TodoExample.Models;

namespace TodoExample.Pages.Todo
{
    public class Index : PageModel
    {
        private TodoContext _db;
        public IEnumerable<TodoItem> Todos { get; set; }

        [BindProperty]
        public TodoItem AddUpdateItem { get; set; }

        [BindProperty]
        public List<SelectListItem> Categories { get; set; }

        public Index(TodoContext db)
        {
            this._db = db;
            Todos = db.Todos.Include(x => x.Category);

            Categories = db.Categories
                .Select(x => new SelectListItem(x.Name, x.CategoryId.ToString()))
                .ToList();
        }
        
        public void OnGet(string? filterCategory, bool? filterDone, string filterDescription)
        {
            if (filterDone != null)
            {
                Todos = Todos.Where(x => x.Done == filterDone);
            }

            if (!string.IsNullOrWhiteSpace(filterDescription))
            {
                Todos = Todos.Where(x => x.Description.StartsWith(filterDescription));
            }

            if (!string.IsNullOrWhiteSpace(filterCategory))
            {
                Todos = Todos.Where(x => x.Category.Name == filterCategory);
            }
        }

        public IActionResult OnPostDelete(int todoId)
        {
            TodoItem todoItemToRemove = _db.Todos.Find(todoId);
            if (todoItemToRemove == null)
            {
                return Page();
            }
            
            _db.Todos.Remove(todoItemToRemove);
            _db.SaveChanges();

            return RedirectToPage();
        }

        public IActionResult OnPostEdit(int todoId)
        {
            TodoItem todoItemToEdit = _db.Todos.Find(todoId);
            if (todoItemToEdit == null) return NotFound();

            AddUpdateItem = todoItemToEdit;
            return Page();
        }

        public IActionResult OnPostCancel()
        {
            return RedirectToPage();
        }

        public IActionResult OnPostUpdate()
        {
            if (!ModelState.IsValid)
                return Page();

            _db.Attach(AddUpdateItem).State = EntityState.Modified;
            _db.SaveChanges();
            return RedirectToPage();
        }
        
        public IActionResult OnPostAdd()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            _db.Todos.Add(AddUpdateItem);
            _db.SaveChanges();
            
            return RedirectToPage();
        }
    }
}